private void MergeSort(long[] inputArray)
{
    int left = 0;
    int right = inputArray.Length - 1;
    InternalMergeSort(inputArray, left, right);
}